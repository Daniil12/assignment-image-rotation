#include "file.h"
#include "bmp.h"
#include "rotate.h"

#include <stdio.h>
#include <stdlib.h>

int error(const char *message) {
    fputs(message, stderr);
    return 0;
}

void info(const char *message) {
    fputs(message, stdout);
}

int main(int argc, char **argv) {
    if (argc != 3) {
        return error("Необходимо 2 параметра\n");
    }

    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        fclose(in);
        return error("Не удалось окрыть входной файл\n");
    }

    struct image img = {0};
    enum read_status rs = from_bmp(in, &img);
    if (rs != READ_OK) {
        destroy(img);
        fclose(in);
        return error("Неверного формат bmp\n");
    }
    fclose(in);

    info("Входной файл успешно прочитан\n");

    struct image img_rotate = rotate(img);
    destroy(img);

    FILE *out = fopen(argv[2], "wb");
    if (out == NULL) {
        fclose(out);
        return error("Не удалось окрыть выходной файл\n");
    }

    enum write_status ws = to_bmp(out, &img_rotate);
    if (ws != WRITE_OK) {
        destroy(img_rotate);
        fclose(out);
        return error("Ошибка конвертации в .bmp\n");
    }
    destroy(img_rotate);
    fclose(out);

    info("Заголовки успешно записаны\n");
    info("Картинка перевёрнута\n");
    return 0;
}
