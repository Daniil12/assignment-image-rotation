#include "../include/rotate.h"

struct image rotate(struct image const source) {
    struct image img = (struct image) {
            .width = source.height,
            .height = source.width,
            .data = malloc(sizeof(struct pixel) * source.width * source.height)
    };

    struct pixel *copy_pixel = img.data;

    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            *(copy_pixel + j * source.height + (source.height - i - 1)) = *(source.data + i * source.width + j);
        }
    }
    return img;
}
